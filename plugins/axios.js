import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'

axios.interceptors.request.use(
  config => {
    console.log('config: ', config)
    return config
  },
  err => {
    console.log('err: ', err)
    return Promise.reject(err)
  }
)
axios.interceptors.response.use(
  response => {
    console.log('response: ', response)
    return response
  },
  error => {
    console.log('error: ', error)
    return Promise.reject(error.response.data)
  }
)

export default () => {
  Vue.use(VueAxios, axios)
}
