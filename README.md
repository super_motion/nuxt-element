
## 簡介

Nuxt.js 是一個基於 Vue.js 的應用框架，也就是說沒有 Vue.js 就沒有 Nuxt.js。

## Nuxt.js 可以讓你

建立一個足夠彈性的新專案
導入你現有的 Node.js 的專案當中
Nuxt.js 已經預設好一個 Vue.js 應用，並且提供舒適的開發流程與體驗，使開發者能方便處理 Server-Side Rendering(SSR) 及 SEO 相關問題。

## Nuxt.js 提供三種部署方式(模式)

* universal: 簡單理解為有 SSR
* spa: 簡單理解為無 SSR
* generate: 將應用產出為靜態 HTML 頁面，即可部署於 github-page 等。

## Nuxt.js 依賴的框架/套件

* Vue 2
* Vue Router
* Vuex
* Vue Server Renderer (mode = 'spa' 除外)
* vue-meta
* 此外，Nuxt.js 2.0 使用 webpack 4、vue-loader、babel-loader 來處理程式碼的自動建置，當然這些設定是可以透過 nuxt.config.js 設定檔去客製化擴充以及調整的。

## Nuxt.js 特性

1. 以 *.vue 構成
1. 代碼分層 (Code Splitting)
1. 服務端渲染: 透過 vue-server-renderer
1. 強大的路由功能，支持異步數據: 透過 vue-router 搭配目錄結構的設計，基本上可以不用再寫 router
1. 靜態文件服務: 上一段提到的 nuxt generate 指令
1. ES6/ES7 語法支持: 透過 babel
1. 打包和壓縮 JS 和 CSS: 透過 webpack，之後章節會分享效能優化的經驗
1. HTML 頭部標籤管理: 透過 nuxt.config.js 和 vue-meta
1. 本地開發支持熱加載(hot reload)
1. 集成 ESLint
1. 支持各種樣式預處理器: 本系列文章將使用 Stylus
1. HTTP/2 push headers ready
1. 使用模塊化 modules 的方案擴充 Nuxt 應用： nuxt-community/modules 提供許多大神們寫好的擴充套件，例如: vuetify、markdownit、gtm、firebase等等，已經幫我們處理完整合的部分，只需無腦安裝就可以使用。

## 目錄結構

``` 
├── README.md
├── assets    資源目錄，未編譯的靜態資源如less、js
├── components    元件目錄
├── config    環境變數
├── layouts    佈局目錄
├── middleware
├── node_modules
├── nuxt.config.js
├── package-lock.json
├── package.json
├── pages    頁面目錄
├── plugins    外掛
├── static    靜態檔案目錄
├── store    vuex store
├── utils    工具方法
└── yarn.lock

```
## 項目部署

![Imgur](https://i.imgur.com/f9UKSi3.png)


| 命令           | 描述                                      |
| ------------- | ---------------------------------------- |
| npm run dev   | 啟動一個熱載入的 Web 服務器（開發模式） [localhost:3000](http://localhost:3000/) |
| npm run start    | 利用 webpack 編譯應用，壓縮 JS 和 CSS 資源（發佈用）      |
| npm run build   | 以生成模式啟動一個 Web 服務器 (`nuxt build` 會先被執行)   |
| npm run generate | 編譯應用，並依據路由配置生成對應的 HTML 檔案 (用於靜態網站的部署)    |

### 靜態化頁面部署

我們從官網給出的文檔可以看出，部署靜態化頁面需要用到的命令是 `nuxt generate` ，執行的時候會在根目錄下面生成 dist 目錄，裡面的檔都是靜態化頁面需要的打包好的檔。

**這裡需要特別注意的一點是，如果你的專案中存在 axios 請求的話，記得在你本地開啟一個本機服務哦 ~ 不然打包的時候執行到 axios 請求的時候會報錯。 因為前面我們通過使用 node 的 process 對運行環境進行了是跳轉還是請求的判定，而打包進行請求的時候是不依賴 node 環境的**

### 管理 vuex

Vuex 架構參考: https://vuex.vuejs.org/zh/guide/structure.html

```shell
├── actions.js                  
├── index.js				
├── mutations.js                
├── state.js
```

### 註冊 Global plugin

`Nuxt` 的專案不比 `vue` 的專案，提供了主入口檔供我們對通用群組件進行配置。 但要做到這個點也比較簡單，我們只需要按照 `Nuxt` 官網給出的規範來，將元件引入的相關配置寫入到 plugins 目錄下即可

比如，我需要引入三方元件庫 [element-ui](HTTPs://github.com/ElemeFE/element) ，我們只需在 plugins 目錄下新建一個 `element-ui.js` 檔，並寫入

```javascript
import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

Vue.use(ElementUI)
```

然后在 `nuxt.config.js` 文件中引入

```javascript
plugins: [
  '~/plugins/element-ui'
]
```

最後你就可以在你的專案中使用 `element-ui` 的元件了。

當然，你想配置自己本地的通用群組件，也是一樣的做法。 先在 plugins 目錄下新建一個 js 檔，然後引入你的檔，最後再在 `nuxt.config.js` 檔中引入即可。

### Nuxt config 設定

    {
      mode: 'spa' ｜ universal,
      /*
      ** Headers of the page
      */
      head: {
        title: pkg.name,
        meta: [
          { charset: 'utf-8' },
          { name: 'viewport', content: 'width=device-width, initial-scale=1' },
          { hid: 'description', name: 'description', content: pkg.description }
        ],
        link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
      },
      scrollToTop: true,
      /*
      ** Customize the progress-bar color
      */
      loading: { color: '#fff' },

      /*
      ** Global CSS
      */
      css: [
        './node_modules/normalize.css/normalize.css',
        './node_modules/font-awesome/css/font-awesome.min.css',
        'element-ui/lib/theme-chalk/index.css',
        'element-ui/lib/theme-chalk/display.css'
      ],

      /*
      ** Plugins to load before mounting the App
      */
      plugins: [
        '@/plugins/element-ui',
        '@/plugins/filters.js',
        '@/plugins/axios',
        '@/plugins/mock'
      ],
      env: {
        ENV: require('./config/env').NODE_ENV,
        BASE_URL: require('./config/env').BASE_URL
      },
      /*
      ** Nuxt.js modules
      */
      modules: [
        // Doc: https://github.com/nuxt-community/axios-module#usage
        // '@nuxtjs/axios'
      ],
      /*
      ** Axios module configuration
      */
      axios: {
        // baseURL: undefined,
        /**
         * Adds an interceptor to automatically set `withCredentials` config of axios
         * when requesting to baseUrl which allows passing authentication headers to backend.
         */
        // credentials: false,
        /**
         * Adds interceptors to log all responses and requests
         */
        // debug: true,
        /**
         * This option is a map from specific error codes to page which they should be redirect.
         *
         * For example if you want redirecting all `401` errors to `/login`
         */
        // redirectError: {
        // 401: '/login'
        // }
        /**
         * Function for manipulating axios requests.
         *
         * Useful for setting custom headers, for example based on the store state.
         * The second argument is the nuxt context.
         */
        // requestInterceptor: (config, { store, req }) => {
        //   return config
        // },
        /**
         * Function init(axios, ctx) to do additional things with axios.
         *
         * Example:
         *
         *    init (axios, ctx) {
         *      axios.defaults.xsrfHeaderName = 'X-CSRF-TOKEN'
         *    }
         */
        // init: (axios, ctx) => {},
        /**
         * If you want to disable the default error handler for some reason,
         * you can do it so by setting the option `disableDefaultErrorHandler` to `true`.
         */
        // disableDefaultErrorHandler: true
        /**
         * Function for custom global error handler.
         *
         * If you define a custom error handler, the default error handler provided by this package will be overridden.
         */
        // errorHandler: (errorReason, { error }) => {
        //   error('Request Error: ' + errorReason)
        // }
        // See https://github.com/nuxt-community/axios-module#options
      },
      // cache component
      render: {
        bundleRenderer: {
          cache: require('lru-cache')({
            max: 1000,
            maxAge: 1000 * 60 * 15
          })
        }
      },
      /*
      ** Build configuration
      */
      build: {
        /*
        ** You can extend webpack config here
        */
        plugins: [
          new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
          })
        ],
        postcss: [
          postcssNested(),
          postcssResponsiveType(),
          postcssHexrgba(),
          autoprefixer({
            browsers: ['last 3 versions']
          })
        ],
        babel: {
          plugins: [
            [
              'component',
              {
                libraryName: 'element-ui',
                styleLibraryName: 'theme-chalk'
              }
            ]
          ]
        },
        extend(config, ctx) {
          // Run ESLint on save
          if (ctx.isDev && ctx.isClient) {
            config.module.rules.push({
              enforce: 'pre',
              test: /\.(js|vue)$/,
              loader: 'eslint-loader',
              exclude: /(node_modules)/,
              options: {
                fix: true
              }
            })
          }
        }
      }
    }