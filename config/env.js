const isProdMode = Object.is(process.env.NODE_ENV, 'production')

module.exports = {
  NODE_ENV: isProdMode ? '"production' : '"development"',
  BASE_URL: isProdMode ? '"' : '"http://127.0.0.1:3000/"'
}
